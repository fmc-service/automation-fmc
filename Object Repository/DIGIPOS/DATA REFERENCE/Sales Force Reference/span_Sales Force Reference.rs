<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Sales Force Reference</name>
   <tag></tag>
   <elementGuidId>f8d243a5-5768-41dc-9ae0-b40baf5aeeef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.ant-menu-item.ant-menu-item-active.ant-menu-item-selected > span.ant-menu-title-content</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>// span[text()=&quot;Sales Force Reference&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>1071bf6a-a44e-49e9-a891-72f579e56c35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-menu-title-content</value>
      <webElementGuid>45d9d8e4-1a37-41d6-9120-d2744afed494</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sales Force Reference</value>
      <webElementGuid>50ea221e-1184-4a73-b379-32ac4da8e58f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;rc-menu-uuid-62488-1-IE-REF-DR-popup&quot;)/li[@class=&quot;ant-menu-item ant-menu-item-active ant-menu-item-selected&quot;]/span[@class=&quot;ant-menu-title-content&quot;]</value>
      <webElementGuid>34afe4d3-b377-4764-a75e-9f15b21da382</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='rc-menu-uuid-62488-1-IE-REF-DR-popup']/li[3]/span</value>
      <webElementGuid>f7274e06-0f76-4444-8826-aa0384689cf5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='City Sales Reference'])[1]/following::span[1]</value>
      <webElementGuid>7c4f2fa7-3cdd-4cc9-880f-0e9c07fb8019</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Territory Reference'])[1]/following::span[2]</value>
      <webElementGuid>5e79c5ab-1317-40c7-bd42-51fb08941dfb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Outlet Reference'])[1]/preceding::span[1]</value>
      <webElementGuid>3d9d4cf0-a71e-40fe-bb71-b8422f401f0e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Channel Mapping'])[1]/preceding::span[2]</value>
      <webElementGuid>47e7c737-2daa-481f-86da-749eb2accf40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sales Force Reference']/parent::*</value>
      <webElementGuid>7f70cbe0-3d44-4144-8bfd-245b023ea93a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/span</value>
      <webElementGuid>a9d152be-c91a-4247-b5c7-18a1adbda8eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Sales Force Reference' or . = 'Sales Force Reference')]</value>
      <webElementGuid>d0bb757b-6732-4c2e-9a94-32ce56cf7044</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
