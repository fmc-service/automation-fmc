<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button next</name>
   <tag></tag>
   <elementGuidId>632c9e2b-409a-4c8d-ab64-e2919ec62254</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h3.ant-typography.css-byeoj0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[text()=&quot;Next&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>ca1c4591-05a9-40f6-af92-91e60989f49f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-typography css-byeoj0</value>
      <webElementGuid>facf2987-af26-49c0-a962-a7f8ae96eb4f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Data Reference</value>
      <webElementGuid>8e1b8cbf-b2cc-4b81-bcbc-e256871a7721</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@id=&quot;v-1.6.68&quot;]/div[@id=&quot;v-1.6.68&quot;]/div[@class=&quot;ant-layout ant-layout-has-sider css-byeoj0&quot;]/div[@class=&quot;ant-layout css-byeoj0&quot;]/div[@class=&quot;ant-row css-byeoj0&quot;]/div[@class=&quot;ant-col css-byeoj0&quot;]/h3[@class=&quot;ant-typography css-byeoj0&quot;]</value>
      <webElementGuid>42cc7a0e-3ed1-421f-8feb-961007b33d5a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='v-1.6.68']/div/div/div/div/h3</value>
      <webElementGuid>453de022-7e8e-41c3-845d-e7719f6660c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mapping Role Menu'])[1]/following::h3[1]</value>
      <webElementGuid>8afe074a-1d88-4f06-9082-df77e46ca949</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='User Role'])[1]/following::h3[1]</value>
      <webElementGuid>13d85fe9-d548-47d5-a8f6-546f6e76a245</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Refresh'])[1]/preceding::h3[1]</value>
      <webElementGuid>8e76e6b0-73cc-4a2c-81ef-42c7dc11d18f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sales Force Reference'])[2]/preceding::h3[1]</value>
      <webElementGuid>4625210e-5d5b-43c4-9784-41f2ba715e11</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3</value>
      <webElementGuid>bce643b7-02e9-41bb-94fa-361462756a19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Data Reference' or . = 'Data Reference')]</value>
      <webElementGuid>bc8cffbf-f316-4271-90b3-df6849d08ca6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
