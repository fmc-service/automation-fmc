<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button previous</name>
   <tag></tag>
   <elementGuidId>c69d793c-2dba-464e-a027-046a9688582e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h3.ant-typography.css-byeoj0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[text()=&quot;Previous&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>6b1d8677-b0fc-4bc5-8182-6c6b8ea3d4a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-typography css-byeoj0</value>
      <webElementGuid>5f8c2589-3aa1-4f8e-818f-98e9b81115ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Data Reference</value>
      <webElementGuid>a9bef22d-1a14-432a-99d3-f942d296358d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@id=&quot;v-1.6.68&quot;]/div[@id=&quot;v-1.6.68&quot;]/div[@class=&quot;ant-layout ant-layout-has-sider css-byeoj0&quot;]/div[@class=&quot;ant-layout css-byeoj0&quot;]/div[@class=&quot;ant-row css-byeoj0&quot;]/div[@class=&quot;ant-col css-byeoj0&quot;]/h3[@class=&quot;ant-typography css-byeoj0&quot;]</value>
      <webElementGuid>4da04a6a-5de5-4c16-b501-ceb92d12d811</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='v-1.6.68']/div/div/div/div/h3</value>
      <webElementGuid>ef828703-96fa-4779-ab7d-50251e0b192a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mapping Role Menu'])[1]/following::h3[1]</value>
      <webElementGuid>ff9966b9-aeb9-4e02-9e65-4b0df35abc0b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='User Role'])[1]/following::h3[1]</value>
      <webElementGuid>d22cbe48-d895-497a-8e89-4cb57b229e3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Refresh'])[1]/preceding::h3[1]</value>
      <webElementGuid>669b498e-a109-4d77-aeb6-5bf3ff551674</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sales Force Reference'])[2]/preceding::h3[1]</value>
      <webElementGuid>4afb8821-bd0b-48a6-a45f-6823ccd50d16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3</value>
      <webElementGuid>5ecae66e-5fe2-4327-b1c9-530eb42b5563</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Data Reference' or . = 'Data Reference')]</value>
      <webElementGuid>c6cc55e7-cb94-4583-b8d1-1173bad62867</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
