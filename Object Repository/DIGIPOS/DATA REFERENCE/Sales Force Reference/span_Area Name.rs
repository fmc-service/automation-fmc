<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Area Name</name>
   <tag></tag>
   <elementGuidId>315d28ad-6287-450e-b4f8-ffede5671b54</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='v-1.6.68']/div/div/main/div/div/div/div/div[2]/div/div/div/div/div/div/div/div/table/thead/tr/th[5]/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.ant-table-column-sorters.ant-tooltip-open > span.ant-table-column-title</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>1b077d37-0a3b-46f5-9055-ea8036ec85c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-table-column-title</value>
      <webElementGuid>d61e3a9d-a094-4908-b030-a72193928bc7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Area Name</value>
      <webElementGuid>9e5587f9-db2e-48bb-aa50-4e9633c932d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@id=&quot;v-1.6.68&quot;]/div[@id=&quot;v-1.6.68&quot;]/div[@class=&quot;ant-layout ant-layout-has-sider css-byeoj0&quot;]/div[@class=&quot;ant-layout css-byeoj0&quot;]/main[@class=&quot;ant-layout-content css-byeoj0&quot;]/div[@class=&quot;ant-card ant-card-bordered css-byeoj0&quot;]/div[@class=&quot;ant-card-body&quot;]/div[@class=&quot;ant-row css-byeoj0&quot;]/div[@class=&quot;ant-col css-byeoj0&quot;]/div[@class=&quot;ant-row css-byeoj0&quot;]/div[@class=&quot;ant-col css-byeoj0&quot;]/div[@class=&quot;ant-row css-byeoj0&quot;]/div[@class=&quot;ant-table-wrapper css-byeoj0&quot;]/div[@class=&quot;ant-spin-nested-loading css-byeoj0&quot;]/div[@class=&quot;ant-spin-container&quot;]/div[@class=&quot;ant-table ant-table-bordered ant-table-ping-left ant-table-ping-right ant-table-scroll-horizontal&quot;]/div[@class=&quot;ant-table-container&quot;]/div[@class=&quot;ant-table-header ant-table-sticky-holder&quot;]/table[1]/thead[@class=&quot;ant-table-thead&quot;]/tr[1]/th[@class=&quot;ant-table-cell ant-table-column-has-sorters&quot;]/div[@class=&quot;ant-table-column-sorters ant-tooltip-open&quot;]/span[@class=&quot;ant-table-column-title&quot;]</value>
      <webElementGuid>cc09f943-2cde-4828-b979-be7f85be2bee</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='v-1.6.68']/div/div/main/div/div/div/div/div[2]/div/div/div/div/div/div/div/div/table/thead/tr/th[5]/div/span</value>
      <webElementGuid>917ef1f4-e912-4746-bded-16a6aebb2832</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama'])[1]/following::span[5]</value>
      <webElementGuid>d8bd10f2-38a2-4920-8c47-c81f4e21704f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SF Code'])[1]/following::span[10]</value>
      <webElementGuid>59d95e60-e521-4d4e-9e7d-31a284fab321</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Regional'])[1]/preceding::span[5]</value>
      <webElementGuid>125e9261-03d1-43b1-b515-88c92dbf20a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Branch'])[1]/preceding::span[10]</value>
      <webElementGuid>0b4c916d-5878-4eb2-a37e-470db2b0f172</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Area Name']/parent::*</value>
      <webElementGuid>0f7f6679-0a67-4cdb-868b-27220edefb32</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//th[5]/div/span</value>
      <webElementGuid>5bc050cb-6fdb-44e2-b6f1-04a5c879976d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Area Name' or . = 'Area Name')]</value>
      <webElementGuid>b21045ec-aa6b-48ad-9707-b77d4f6e4e16</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
