<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_SF Code</name>
   <tag></tag>
   <elementGuidId>a2de3553-457b-4224-88cf-c8010ccb32b5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='v-1.6.68']/div/div/main/div/div/div/div/div[2]/div/div/div/div/div/div/div/div/table/thead/tr/th[3]/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.ant-table-column-sorters.ant-tooltip-open > span.ant-table-column-title</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>2a955f1d-bf78-4049-ba7b-2cd702ecc7ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-table-column-title</value>
      <webElementGuid>24d925f2-c74c-4ac8-bd3d-955ef5a71597</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>SF Code</value>
      <webElementGuid>1889f8bd-298a-4598-8263-84c475e907f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@id=&quot;v-1.6.68&quot;]/div[@id=&quot;v-1.6.68&quot;]/div[@class=&quot;ant-layout ant-layout-has-sider css-byeoj0&quot;]/div[@class=&quot;ant-layout css-byeoj0&quot;]/main[@class=&quot;ant-layout-content css-byeoj0&quot;]/div[@class=&quot;ant-card ant-card-bordered css-byeoj0&quot;]/div[@class=&quot;ant-card-body&quot;]/div[@class=&quot;ant-row css-byeoj0&quot;]/div[@class=&quot;ant-col css-byeoj0&quot;]/div[@class=&quot;ant-row css-byeoj0&quot;]/div[@class=&quot;ant-col css-byeoj0&quot;]/div[@class=&quot;ant-row css-byeoj0&quot;]/div[@class=&quot;ant-table-wrapper css-byeoj0&quot;]/div[@class=&quot;ant-spin-nested-loading css-byeoj0&quot;]/div[@class=&quot;ant-spin-container&quot;]/div[@class=&quot;ant-table ant-table-bordered ant-table-ping-right ant-table-scroll-horizontal&quot;]/div[@class=&quot;ant-table-container&quot;]/div[@class=&quot;ant-table-header ant-table-sticky-holder&quot;]/table[1]/thead[@class=&quot;ant-table-thead&quot;]/tr[1]/th[@class=&quot;ant-table-cell ant-table-column-has-sorters&quot;]/div[@class=&quot;ant-table-column-sorters ant-tooltip-open&quot;]/span[@class=&quot;ant-table-column-title&quot;]</value>
      <webElementGuid>bdc70949-ff7f-4e32-93e5-f98a230a9a38</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='v-1.6.68']/div/div/main/div/div/div/div/div[2]/div/div/div/div/div/div/div/div/table/thead/tr/th[3]/div/span</value>
      <webElementGuid>7269ab9c-906a-4eea-a581-9dfcb529f5af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SF ID'])[1]/following::span[5]</value>
      <webElementGuid>339100e0-000a-4725-88b2-03486e28378c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No'])[1]/following::span[6]</value>
      <webElementGuid>03b9c0fc-9c2b-4842-9c46-3f3c6e0efd89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama'])[1]/preceding::span[5]</value>
      <webElementGuid>549e1cf2-c72e-4732-8800-40337efeefe6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Area Name'])[1]/preceding::span[10]</value>
      <webElementGuid>05632043-a43a-4a68-abff-6138179d01c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='SF Code']/parent::*</value>
      <webElementGuid>35fc9ebb-9dd5-4f55-b5b6-ddbc904b828f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//th[3]/div/span</value>
      <webElementGuid>d439b602-517c-475c-93bf-539e7b0da6c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'SF Code' or . = 'SF Code')]</value>
      <webElementGuid>e0936595-5087-473e-966f-c51593693b19</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
