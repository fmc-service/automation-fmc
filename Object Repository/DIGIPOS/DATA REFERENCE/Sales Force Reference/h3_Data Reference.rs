<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_Data Reference</name>
   <tag></tag>
   <elementGuidId>490be783-76bb-4aac-a5dd-86562f36f208</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='v-1.6.68']/div/div/div/div/h3</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h3.ant-typography.css-byeoj0</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>bb5d932f-d125-4897-8a54-65161c9e3e89</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ant-typography css-byeoj0</value>
      <webElementGuid>ac52c045-bde1-4758-bccf-fea602e963e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Data Reference</value>
      <webElementGuid>76beae44-b67e-4501-a96b-d5807def0f9a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@id=&quot;v-1.6.68&quot;]/div[@id=&quot;v-1.6.68&quot;]/div[@class=&quot;ant-layout ant-layout-has-sider css-byeoj0&quot;]/div[@class=&quot;ant-layout css-byeoj0&quot;]/div[@class=&quot;ant-row css-byeoj0&quot;]/div[@class=&quot;ant-col css-byeoj0&quot;]/h3[@class=&quot;ant-typography css-byeoj0&quot;]</value>
      <webElementGuid>2ea7088a-cb66-4a81-a605-c33350442591</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='v-1.6.68']/div/div/div/div/h3</value>
      <webElementGuid>3edc24ee-903e-48d6-b9dd-f1503774718b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mapping Role Menu'])[1]/following::h3[1]</value>
      <webElementGuid>23c01eac-6ba0-4f6e-b291-18346b8e5676</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='User Role'])[1]/following::h3[1]</value>
      <webElementGuid>b60a3a0d-d740-4ed9-89f5-6e360398d23e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Refresh'])[1]/preceding::h3[1]</value>
      <webElementGuid>3fb86774-3e41-4782-9b32-6d3eb0681237</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sales Force Reference'])[2]/preceding::h3[1]</value>
      <webElementGuid>5375aa15-2aae-497c-ac21-fbf0b7046a29</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3</value>
      <webElementGuid>c78a36db-4b23-41fa-9a24-46d726a3cfc6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Data Reference' or . = 'Data Reference')]</value>
      <webElementGuid>12f19c07-608e-43f3-9bca-93e3c021ad21</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
