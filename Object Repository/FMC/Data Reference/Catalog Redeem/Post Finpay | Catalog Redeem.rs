<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Post Finpay | Catalog Redeem</name>
   <tag></tag>
   <elementGuidId>1da29267-199b-4c65-945f-d37062cbb695</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;${body}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>18906e97-3603-4c79-8e90-94bbcf83fe68</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-authorize-key</name>
      <type>Main</type>
      <value>${GlobalVariable.authToken}</value>
      <webElementGuid>8bfef790-067a-4c61-88bb-8fcf56edd363</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.8</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.stagingFmc}redeem-catalogue/voucher</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>'{\n  &quot;title&quot;: &quot;Hilman Data Dummy Lagi&quot;,\n  &quot;startDate&quot;: &quot;2030-02-08T12:31:51.551Z&quot;,\n  &quot;endDate&quot;: &quot;2030-03-08T12:31:51.551Z&quot;,\n  &quot;coinAmount&quot;: 200,\n  &quot;description&quot;: &quot;this is payload from automation test&quot;,\n  &quot;termsAndConditions&quot;: &quot;this payload is dummy for testing purpose&quot;,\n  &quot;bannerImageName&quot;: &quot;&quot;,\n  &quot;bannerImageBase64&quot;: &quot;/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMHBhUNBxIVFRUVFhYVFhAWFxUfFRASFhUYFhgZFhcZKCggGRolHRUfITIhJSkrLy4uGCszODMtNygtLisBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAwEBAQEBAAAAAAAAAAAABgcIBQEEAwL/xABNEAABAwIDAgYLDQYEBwAAAAAAAQIDBBEFBhIHIRMXMVSS0TI3QVFxcnOBk6GyCBQWIjU2U2GDkbGzwiMzQlLh8Bg0YoIVJENEdKPB/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ALxAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHlz0AALgAeXPQAAAAAAAAAAAAAAAAIttIzM/KOWXV1KxsjkexuhyqiWctu4VNx+1XNIek8nm33tdv8tF7SmfMn4H8JMyw0GvRwquTXa+mzHO5P9oFj8ftVzSHpPHH7Vc0h6Tzo8QCc+/8AX/UpfE6X3liUkCLfg3vZfv6XK2/qAtfj9quaQ9J44/armkPSefJkrZAmaMuR13vrg+E1fE0XtZypy3+o5O0rZz8B6SGVJ+F4Vzm206dOlEXv/WBIeP2q5pD0njj9quaQ9J5CNnmUUzljq0ay8FaJ0mu176XNS1v9x+ef8rfBDMC0SScJZjH67W7K+63mAnfH7Vc0h6Txx+1XNIek8i2zPISZ5knas/BcCka9jfXwmtPNbR6ydcQCc+X0f9QOdx+1XNIek8cftVzSHpPOjxAJz5fR/wBSuNo2UPgXjLaVJeF1MR+rTa11VLeoC/tledpM7YfNNWRMjWN6MRGqq3RW333P0z3tKpcoJwb/ANtPa6QMVPi+O7+H8Ss9lGPfBrZxiFa3smSNRid+RzEa31qVTJLLiuIapFdJLK7eq9k97lAsjENueITSXo44I07iaVcv3rYUG3Svhf8A85HBIndTSrb+dFU7GXthC1NEkmO1CxvciLwUaIui/cc53d8B++K7A0bTquE1aq/uNlamlfC5vJ9wE4yPtOpM2PSFP2M6/wDQeqfGW110O3atyclkX6jk7UdpU2SsXjp6SCORHx67uVyKi6lSyW8BnuqgmwTFVjl1RzQvstuVj296x38/5kXNEFHUVO+VsCxyru+M9j1RXbu/ygTLj9quaQ9J44/armkPSeRrZps9TPEczln4LglanY31ar9R3c47HEy1luav996+CRq6NFtV3o3lv/qA+jj9quaQ9J44/armkPSeVLRwe+KtkfJqc1t+9qW3/wBLrTYAipdK1fR/1A53H7Vc0h6Ty1NmWa35wy8tXVRtjXhHM0tVVSzbb9/hM1Z3y6mVsxSUKScJoRq67WvqbfkL19z38xV8vJ+DQLOAAAAAAABBds+FzYzkh0GFxulkWWNdDU32Rd6lUbMclYhhee6WoxCkljjY5yue5Es1FjeiX86mkFS4A8dyGL8x/OGo8vL+Y42g7kMX5j+cNR5eX8xwGmNina5p/tPbU6eeckwZ0p448SfI1InOc3g1aiqrkRFvqRe8czYp2uqf7T21J0BB8mbMqXJ+LLV4dJM5yxujtI5itRrlaq2s1N/xUKd2/wDbCXyMX6jTRmXb/wBsFfIxfqAknuaP8xXeLT/jMXrcyfs6z8/Iz5nQQNl4ZI0XU5W6eD18lkW99fqJt/iAm5jH6V3UBfSlP7XNndbmzMLKjCEj0NiRi632XUiqvJY4v+ICZf8AsY/Su6i2Nn+Y3ZryyyunjSNXq9NCKqomlypyrYCnsRyRV5X2T1kWJozUtRDKmh2pNDfiqq/eVzk7EWYRmmnqqvsI5Wud9TUXevm5TYGJUDMToH09YmpkjVY5vfRUsZSzxkeoyfiCtqWq+Ff3dQiLoe1eRHL/AAu+oDV2H18eI0rZqGRr2ORFR7Vuiov98h9FzGmCZlq8Al1YPUSRd3S1y6HeMxdy/cWFg23WspURMUhjnTvoqscvnRFT1ANsOUqvEc8yzYVSSyMc2NdbWqqK7TZd/mIV8BMTVP8AIz9BTQ2U9qVBmN6Rtk4GVeSKWyXX/S7kUnCbwKi2BYHU4JDVJi0EkWpY9OtttVkW9iVbYO1vV+Kz81hM7EM2w9rer8Vn5rAMuYT8qxeUj9pDazOwTwGKcJ+VYvKR+0htZnYJ4AIBmnZPSZmxp9bWyzte9G3axzEamlLbrtUkWTcrRZQwpaTDnPc1Xq+71RXXXwIneO8AAAAAAAAAAAA8dyGLsx/OGo8vL+Y42i7kMXZj+cNR5eX8xwGmNina6p/tPbUnRBdina6p/tPbUnQAzLt/7YS+Qi/UaaMy7f8AthL5GL9QHJ2c5Cdnl86QTth4FI1XU1XauE1W5F3W0esm3EBNz6P0Tus/T3NH7+u8Wn/GYvYChOICbn0fondZbGQMuOyplplDLIkisVy60RURdS35FJGAOfjWNQYFScPi0rYmXRut3JqXkT1HHpM3YZmGoSip6iGZ0l0SHl12arl3KneRV8xGfdCfMNP/ACI/ZeUxsnrGUG0OklqVs3W5qr3lfE9ietyAXZjuxjDcTcr6Jr6dy7/2blVl/EddE8CWK/x7YZV0SK/CJY52/wAi3ZJ67ov3miEU9AxRXUUuGVqw1zHRyNXe1yWc1f77pfOwrO8mLwOwzFXq6SJqOikXlfGm5WuXuq3dv7ynx+6RpI/+H01QqJwut0d+6selXWXwL+JX+xWVYtotPo/i1tXwKxeoDVKchDNsHa3q/FZ+awmachDNsHa3q/FZ+awDLmE/KsXlI/aQ2szsE8BinCflWLykftIbWZ2CeAD0AAAAAAAAAAADxeQCvcY2v0GE4pJSVbZtcT1Y6zEtdq2W28zXjFQlXi000V9L5HvS/LZzlVL/AHlp5w2UYji+aqmqo2RqyWV72qsiIuly3TdY43Etiv0cXpU6gLj2Kdrqn+09tSdEW2Z4LLl7J8VHiSIkjFfdGrdN7lVN5KQBmXb/ANsFfIRfqNNFKbV9nFdmjNq1eFMjVixMbdz0RdTb33ecCH7Hc7U2TJalcVSReGSJG6G37DhL36SFmceOG/yz9BOsrLiWxX6OL0qdQ4lsV+ji9KnUBZvHlhv8s/QTrHHlhv8ALP0E6ysuJbFfo4vSp1DiWxX6OL0qdQHb2rbSqPNuWEpMMSVH8Kx/x2oiaWo5F33+sp9jtDkVu5U3oqcqL9RYvEtiv0cXpU6j78B2J1smKtbjiMZCqO1PZIivauhdKond+NbzXA6eRNtfvKjbTZnje/SiNSpZvcqJu+O1eVfrRSYVW23C4oNUDpZHdxiRqnrduK4xPYfiFPVK2gdDKzuP16Vt3NTVTcvgPk4l8V+ji9KnUBwc+5ylzpiqTVLeDY1NMcKLdI0ve6ruu5e/buE59z3lh8+LPxSdqpHG1WRuVP3kjtzlb4qfifvlLYbK+pSTNEjWsRf3EblVz/GduRqeDeXjh1DHhtE2noWIyNiI1rE5Gom4D+MUr24XhklTUIumNrnuROWzUutkKbz9tXocw5QnoqFsyPka1G6mojbo9rt637yKW1mqifiOXKinpURXyRPa1FWyK5U3XXuGeOJbFfo4vSp1AQXCvlWLyjPbQ2szsE8Bmug2OYpT10ckkcVmvY5f2qciORV7hpOPcxNXeA/oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH/9k=&quot;\n}\n'</defaultValue>
      <description></description>
      <id>976e476b-96de-45a0-85e4-38d0014d962d</id>
      <masked>false</masked>
      <name>body</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
