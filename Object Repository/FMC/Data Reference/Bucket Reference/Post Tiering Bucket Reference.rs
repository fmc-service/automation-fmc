<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Post Tiering Bucket Reference</name>
   <tag></tag>
   <elementGuidId>242c3ffe-7cca-43f7-83fe-a9a2d3c7c180</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;${body}&quot;,
  &quot;contentType&quot;: &quot;text/plain&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>045e2883-89e5-4f67-a366-dad51867b2cf</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-authorize-key</name>
      <type>Main</type>
      <value>${GlobalVariable.authToken}</value>
      <webElementGuid>841a868c-f685-4703-b6fd-4e2648d8085e</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.8</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.stagingFmc}reference-bucket</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>'{\n  &quot;bucketName&quot;: &quot;this is aasdsaxxd&quot;,\n  &quot;bucketType&quot;: &quot;KOIN&quot;,\n  &quot;imageName&quot;: &quot;Testing.png&quot;,\n  &quot;imageBase64&quot;: &quot;iVBORw0KGgoAAAANSUhEUgAAAUwAAACYCAMAAAC4aCDgAAAAkFBMVEXaICj////cLTTYAADaGiPhVVraHSbZFiD65ubYAAf87/DhYGTYAA3jaGvYABLxwMHkb3L21dbZDRnusLHsoKLYAAvtpaf++vr43d799fX319jgUFXxubvmen3kcXX0ycroiIvdOD7fSE3rm53pjI/qk5bzxcfngYTcMzreQkj54uPbJy/fSk/jZGjuq63xtrh9EI6GAAAMsElEQVR4nO2daWPqKhCGVRCMjVSpcalbrVq30/b//7ubRWsMMxESWhNv3o/nWCVPWIZhZqjVKlkSbd67BQ+kCqZFVTAtqoJpURVMi6pgWlQF06IqmBZVwbSoCqZFVTAtqoJpUVZg8oQsfOVfiNputw2YrHuteTlo0v38ut2fedttASbj9YRI7u/8C8lNotntvO22AnNYTpirRLNbRYBJywlT9IsI81jOYS6+CgiT7soJ0/0uIsz3csL0FgWEyTvlhElGBYSpmBhlgdkuIEzxUlKYyWYXASZ5LSVMuiwiTDdpZpYDJp8XECafJBtVDpiKzV4EmMqurCQwnaRlVASYioVREphkXDyY6v6nHDDpP6XZ94epTj3lgAm0+/4w1dFSDpikVTyY8k1lWQaYgA1yf5hQxywDTPJcPJjeAGBZApjQsnlvmFzxvpUEJvkoHEwmk46XksCUn1Cz7wqTkR7IsvAwKYVm+rvCZETZkJUDJgM2bXeGySXSL7PBZFF8BWVZ2+N/RaDbH6PYgLobTEYm8HyZASaXLnFry2Z3Nv98b0jiSlOijAuPCHY81rjn/zlN+6xoHJBm3wkmE1Jxr2eEyV1vsnk+XCax8WG6PRITntKRncGiFX7FsD2art8dF4l0YZKs0WbfAyaVpAaalxlgcjKZQovBYcOc1P71IybITBm242mTSOWTXBKxxsfTH8OUUriELNfoZHmGCUt5PEo68FIQ6HlJNHBK2Vcc/aHaa3/cB9PRj46fq/SGt5B2E91RYgKTb1aD6Svc9usHATUeJGi6e+X06BrnUdxqfcqgrQ83/uy5G59+XaPZSLvb44YmTROYrkZ70vRyxYYR1UWf1Dq9c8ol3rEDjbekkbPNoQoPk/Nbk0WgXi0lZpIoR2KKRs2cbQ5VdJh8l7ISxDSeoENdo2dbUsFh8qXOHBaq4yIs0y0Kmyo2TL7TZlmvz0Gaf9cvCw6TMtDRgKkDjHQxy9kYExUZJnOwDR2iibIK8X3OthipyDDJ1PAPh8ekhQQch/2iCgxTbI3/8jWx0YNPS35NxYVJs9jRfe+q0dAJzi+quDAxb2K63uPTJniC84sqLEx5e9sC6RAb6PRPV596gWGCB+0aert4SRzgzPtHw5be1spERYUpstraQ3F+IlZL+Yll4DL7t7K72BcUJpP4f7c/vqcLvN8Ozqa7mpBw1nASeZmoIDODLdZNFRQmFDMXqdcknnAdb46OUn56JORs0df+MhVIargxSFMxYTKBdZgNOS3XEp0R+1HXZKhp9RbfdlLPHs1iwpSYY3x9WawZZviMI1YSs/kTGc70aG2k7+4Jc9SD9LqR2C7wI86BOchI34aj2MU2o6uEP0QCvpD25MlXE4qDrNdb4X8C0gVkApN+Jn6kiYR0NIkDSXLM730deiC78KdGIXJ0W95MukOIOtDbJIh0EGCoUb1FknUmDMtNGJ1OJn8ECnIM9IT8vId0qhfv+nMAhVB7CtVdOEsJBgG6ZjQVqNmeJ5gGLCDlioJzzWAyicxiy4RTCJsWA+tIzSE+S4lbAJa7x4Gp5oRFOiQfgjH4gwEKBISvnXKS6SntexyY6rNF6iuedMwZ8k7hIPpQMyXIQf3s48B0kFGufhzIegu1kuh/JUyC6OGUXJ+HgYmu5WocIH+CP/lKUvZQ9X3yZ9XF6mFgYhiAckLoLkcygTvZ2yL5u25yq/8wMLGJsOeon3UQh0eHp/TMemvnXndyxfx/HJgIgilwLo5Zml8CnzMDraQX/23Zf/mK66UfLlLlh0mBnLBQA+BYXM1fjvRKcAdcpGlHECHPBrwU14oW/PLDRG2ajWLS4N9cJ9COO6He1+y9RogrOeKgKD9MtS5KCkyRLOB01lJqxrWNe9NN50gEFJNYfpjoqeT2FJ/rOI4XyPWFRmXN5FIPZqTDYK8GLD8CTMyF3m61DqPRa6/XWywWHx/Pz9Pp9Bvzpg+EZ+imHG0VnA8A04wBomdPrfxzS+1ZIgC59DAtRWEcSIrVjmq0u7IYSg8T2yEaauyhu9JUbeOgyg8zWySHIsmUPaKWvmKkSg9Twz7U0o4qRUT19HxBVX6YqB/STE2uFnHT0/SHVflhpqQ/mWjG02ONUtQ/w6pgnrSVNaoUOdZU52RwVjBPWvtARNYp47RZLz9M8+BrUKugd6E1GG7odLJRfpiWVvPw9I056UmTqKJAhdLDtGVnRt5PJo03laFGj+Fp53BIirFOrmRGMCddusKmlR4mUGIxk9ZnFxDpZtkJ9QJgpYeJh1Wa6RJqIN2UszVUQdxH6WHWRIYnB9S9fDcT7sY4eD1YwMoP044/MzKNzmKSTL7MeB7weKUywUQ97bmegQuye3s2SFdZ0keAicWYtwkzEPQM0iXHz35KpkZc/n70AWBilmHu27YCMS4cspt/3Tbmp+4DwMQCCu3ADEX9MU9nNxIr/UmznDCvYrYxn24KTFeJi+c15iX/7eq0zF+TWPopkWS3YMrkL9yqr3TSr8LkT7GLCDtYlsQYhQkYAP4GCP25s5hgaZnDTX4Dpqv0bc3B86swHb0NNFIeBrqCxLfZ1RDtrjKrUJJS928rb8FUXrvmBPC7MPVSy/dIWS1o09TkKkwoviYlq70vHhmmGoweCTrUlVSF+Q1MaSne+MFDw5x68NcD0YOBTar84wh6VCxHKYjxfGSYQyB0OOSh/nnPgdJaoEfFndGP3TPjVQ5iYhQCAcEEXX5o7YmCzplYFKUpTNg4gmKEOxyC+QJMmtCriLQxX83BiURVPpiIAWIKEzBuanA1D79jAjChl4HXXOjetDMVmAtkIkooH0xkq2EMExpGAuiYrwROBQReBl7J4x9FjlBwmFAOA6BcMLFgfWOY9ZXy6jkUgriSMExgGOLHJP52Eg6kO8NUvQhqQiKofDCRYABzmPXPRHOpgHy+vnkPJ6l21LxJbDU/EGxxGp9MNLUICGYJ24SJZdh2TjBRFyagz6u+yQXkSwu6Dgyz7SYdnVhue1jDBlmcoq0YMEFgezSbMDHz43yC6CDPA2p1CT2nzh50nQdHFkj69HdioDP0yGnO0UpVL1GpBfWcTtM3l+8KGyQvfJEahYKpPSOOkFIK0kCshAbFf3Nz/cCowy+MN8Ic1R0iJVFXp2SRxF+BiZ7rRPm1wA7mRqbEor+drVFvefiOMJh+z46NRYKU+Th9CZqT9LzdAhsRKIfOPkwsK6/dcPxXDGQ5Zjnp/lEYKITCrPeWhAc8GRUEzwkMRrlp4JNSSuVXYEoUzvd6Aw2lSaZ6j5Ei+weHWa9/zHcuIfTfCj+sHIcWo1lExFD3OCPnHWqmKRS1HEXoP/ktmMGDj9PPKE8Wo4mdUf/WM9nzwsTXTFgtkq0UaaBe1D9uwLylU7pvWta6It1RnveqRAOzPNCXyBrd/2Pr5YN5XkpMmqF/aJkTpmFwtf+Os9ZefjkZ9blgXopwGoyQrd72Jz9MvP4VpPAdZ6sK/nM+mAvmhYu+DdzW8xhZgIkaR6DCnVG2gf5T9z4PzLjxrb0E6XfM3DBN1vNxNMiwunlp2vwcE+WBGa+dr9s1TcI88l/JrW9k/GzZjbNYYql6OWDOr/qY5qypvZTbgKkfq355x6Y3pvRit5hlhzlIFCvVMtxfTOKPcsPUXxdj132kbPcAjUTMv5YZ5nMSi05mklksV36Yutcr9OOrIjFIsep5cV9lVphqpbiaRsKGckNJOorcMDUv/uhdP4yY6OZKTK9vKgRg6szaU6iL3Ux0+2cwYdaswKylRUmd1RIJVzjnekbVNun1VWHusHK9F63A4Ypf3RypqW8VhbIBUyOVsV1Txgsj89udc7RMuhIBmO+S7FOvGBp+IoZ3amrW+N2QpR2YaDXwsw4MrMGFe/BOj7NV752EYNIa9Wq4323KcSr45J16SyMsKzB9mqmlHj7AuleB04n28d7Z3ggQQnfevdI8rEnPhNcFB0hvkno9rGiAfzV807ncNiE7MP033MGpvOFP4yOYw3PEYo7dJK1U/j5/jJNj8hrh4fc7udHBKFGn3GFfmg7x8KsswfTHLOIOWjTST1C4I7sv18c+o5e5dIwHWS0MaBdPq4/wgu9ha9F/Iq5G/+KksYm9hPHHzEkul3qyBrPGXNpXp63F5PZwYVwQsuxsN/1Bf7PtLAkRWH1BDXHpnYrLeVL3hTDpkP183R+s3ro74mV5j4HswQyvGp8M4rdM9zZH7Q5GuZQiOOnl5nOVFYUNyPfzNmHWwuQ70fjcrjeb9WwvsFnvUWUZZiDmv+I79rA76hdg/n9VwbSoCqZFVTAtqoJpURVMi6pgWlQF06IqmBZVwbQoH2ajkiUdn/4DcJju7YwcevwAAAAASUVORK5CYII=&quot;,\n  &quot;description&quot;: &quot;Test perubahan&quot;\n}'</defaultValue>
      <description></description>
      <id>86ba1627-3dfd-434d-8951-d8171e921dc5</id>
      <masked>false</masked>
      <name>body</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
