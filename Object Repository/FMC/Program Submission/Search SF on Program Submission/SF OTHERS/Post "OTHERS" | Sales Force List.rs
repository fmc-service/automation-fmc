<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Post &quot;OTHERS&quot; | Sales Force List</name>
   <tag></tag>
   <elementGuidId>1a368498-6315-4d91-a2a7-8895bef7cf75</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\r\n  \&quot;nationality\&quot;: \&quot;OTHER\&quot;,\r\n  \&quot;area\&quot;: \&quot;Lodaya\&quot;,\r\n  \&quot;region\&quot;: \&quot;Bandung\&quot;,\r\n  \&quot;branch\&quot;: \&quot;Kacaban\&quot;,\r\n  \&quot;cluster\&quot;: \&quot;cluster\&quot;,\r\n  \&quot;listMode\&quot;: \&quot;BLACKLIST\&quot;\r\n}&quot;,
  &quot;contentType&quot;: &quot;text/plain&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <katalonVersion>8.6.8</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${Staging FMC}fmc-sales-force-segment/search-sales-force-segment/search</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.Staging FMC</defaultValue>
      <description></description>
      <id>215ff198-8b7d-4d76-b925-b445c73535ce</id>
      <masked>false</masked>
      <name>Staging FMC</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
