<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Post &quot;ALL&quot; | With Upload</name>
   <tag></tag>
   <elementGuidId>639efc59-c4da-41c2-8616-0947567c47ce</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\r\n  \&quot;nationality\&quot;: \&quot;ALL\&quot;,\r\n  \&quot;listMode\&quot;: \&quot;WHITELIST\&quot;,\r\n  \&quot;csvBase64\&quot;: \&quot;cGFydG5lcjEsCnBhcnRuZXIyLApwYXJ0ZXIz\&quot;\r\n}&quot;,
  &quot;contentType&quot;: &quot;text/plain&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <katalonVersion>8.6.8</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${FMC}fmc-sales-force-segment/search-sales-force-segment/search</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.FMC</defaultValue>
      <description></description>
      <id>b06b1291-2bd6-44ce-9113-3056e887edee</id>
      <masked>false</masked>
      <name>FMC</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
