<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Put Briefing</name>
   <tag></tag>
   <elementGuidId>b4fdad13-cab7-4a68-a3e7-e991acb2d777</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\r\n  \&quot;programType\&quot;: \&quot;CLOCK_IN_BRIEFING\&quot;,\r\n  \&quot;programName\&quot;: \&quot;IMAM PUT TEST 1 BRIEFING\&quot;,\r\n  \&quot;startDate\&quot;: \&quot;2024-01-01T13:20:56\&quot;,\r\n  \&quot;endDate\&quot;: \&quot;2024-06-30T13:20:56\&quot;,\r\n  \&quot;channelId\&quot;: \&quot;160\&quot;,\r\n  \&quot;nationality\&quot;: \&quot;Others\&quot;,\r\n  \&quot;listMode\&quot;: \&quot;WHITELIST\&quot;,\r\n  \&quot;area\&quot;: \&quot;Area 1\&quot;,\r\n  \&quot;region\&quot;: \&quot;Region 1\&quot;,\r\n  \&quot;branch\&quot;: \&quot;Branch 1\&quot;,\r\n  \&quot;cluster\&quot;: \&quot;Cluster 1\&quot;,\r\n  \&quot;sfSegmentUploadId\&quot; : \&quot;2c9380828bb89dc6018bc1467d390004\&quot;,\r\n  \&quot;uncheckPartnerIds\&quot;: [],\r\n  \&quot;fmcIncentiveRuleDetailRequests\&quot;: [\r\n        {\r\n            \&quot;programType\&quot;: \&quot;CLOCK_IN_BRIEFING\&quot;,\r\n            \&quot;bucketId\&quot;: \&quot;2c9380828b80eddb018b84b172fe0008\&quot;,\r\n            \&quot;transactionType\&quot;: \&quot;Fix\&quot;,\r\n            \&quot;amount\&quot;: 50,\r\n            \&quot;baseOn\&quot;: \&quot;BRIEFING_LOCATION\&quot;,\r\n            \&quot;clockInRule\&quot;: \&quot;DAILY\&quot;,\r\n            \&quot;allowedClockInNumber\&quot;: 5\r\n        }\r\n    ]\r\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>accept</name>
      <type>Main</type>
      <value>*/*</value>
      <webElementGuid>1de62257-807e-4e00-b351-b891d363cdcf</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>2fca059f-f0c6-47b1-8745-63047952fda3</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.8</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>${Staging}program/2c9380828bd29c60018bd2d31e740004</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.Staging</defaultValue>
      <description></description>
      <id>0aedc856-f5b3-4f17-9916-a030657a4866</id>
      <masked>false</masked>
      <name>Staging</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
