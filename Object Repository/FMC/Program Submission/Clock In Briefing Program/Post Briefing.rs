<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Post Briefing</name>
   <tag></tag>
   <elementGuidId>52623246-fc80-4b2d-a38d-d12935705b13</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\r\n    \&quot;programType\&quot;: \&quot;CLOCK_IN_BRIEFING\&quot;,\r\n    \&quot;programName\&quot;: \&quot;IMAM POST TEST BRIEFING 2\&quot;,\r\n    \&quot;startDate\&quot;: \&quot;2024-01-01T13:20:56\&quot;,\r\n    \&quot;endDate\&quot;: \&quot;2024-06-30T13:20:56\&quot;,\r\n    \&quot;subChannelId\&quot;: \&quot;4072b3ce-341f-4a42-a28f-f86937e31ff6\&quot;,\r\n    \&quot;nationality\&quot;: \&quot;Others\&quot;,\r\n    \&quot;listMode\&quot;: \&quot;WHITELIST\&quot;,\r\n    \&quot;area\&quot;: \&quot;Area 1\&quot;,\r\n    \&quot;region\&quot;: \&quot;Region 1\&quot;,\r\n    \&quot;branch\&quot;: \&quot;Branch 1\&quot;,\r\n    \&quot;cluster\&quot;: \&quot;Cluster 1\&quot;,\r\n    \&quot;sfSegmentUploadId\&quot;: \&quot;2c9380828bb89dc6018bc1467d390004\&quot;,\r\n    \&quot;uncheckPartnerIds\&quot;: [],\r\n    \&quot;fmcIncentiveRuleDetailRequests\&quot;: [\r\n        {\r\n            \&quot;programType\&quot;: \&quot;CLOCK_IN_BRIEFING\&quot;,\r\n            \&quot;bucketId\&quot;: \&quot;2c9380828b80eddb018b84b172fe0008\&quot;,\r\n            \&quot;transactionType\&quot;: \&quot;Fix\&quot;,\r\n            \&quot;amount\&quot;: 555,\r\n            \&quot;baseOn\&quot;: \&quot;BRIEFING_LOCATION\&quot;,\r\n            \&quot;clockInRule\&quot;: \&quot;DAILY\&quot;,\r\n            \&quot;allowedClockInNumber\&quot;: 2\r\n        }\r\n    ]\r\n}&quot;,
  &quot;contentType&quot;: &quot;text/plain&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <katalonVersion>8.6.8</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${Staging FMC}program</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.Staging FMC</defaultValue>
      <description></description>
      <id>5192ee42-f768-482b-9b59-f0a5b8c88114</id>
      <masked>false</masked>
      <name>Staging FMC</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
