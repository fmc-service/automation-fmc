<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Post Survey | Limit NA</name>
   <tag></tag>
   <elementGuidId>b50bbecc-8135-4e39-bf2b-979c399b0782</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\r\n    \&quot;programType\&quot;: \&quot;COMPLETION_CUSTOMER_SURVEY\&quot;,\r\n    \&quot;programName\&quot;: \&quot;Test Imamsdad\&quot;,\r\n    \&quot;startDate\&quot;: \&quot;2033-11-11T02:06:45\&quot;,\r\n    \&quot;endDate\&quot;: \&quot;2033-11-11T02:06:45\&quot;,\r\n    \&quot;subChannelId\&quot;: \&quot;4072b3ce-341f-4a42-a28f-f86937e31ff6\&quot;,\r\n    \&quot;nationality\&quot;: \&quot;ALL\&quot;,\r\n    \&quot;listMode\&quot;: \&quot;WHITELIST\&quot;,\r\n    \&quot;sfSegmentUploadId\&quot;: \&quot;2c9380828c824067018c85ac7e3b0006\&quot;,\r\n    \&quot;uncheckPartnerIds\&quot;: [],\r\n    \&quot;fmcIncentiveRuleDetailRequests\&quot;: [\r\n        {\r\n            \&quot;programType\&quot;: \&quot;COMPLETION_CUSTOMER_SURVEY\&quot;,\r\n            \&quot;bucketId\&quot;: \&quot;2c9380828b80eddb018b848d4f890006\&quot;,\r\n            \&quot;transactionType\&quot;: \&quot;FIX\&quot;,\r\n            \&quot;amount\&quot;: 999,\r\n            \&quot;baseOn\&quot;: \&quot;CUSTOMER_SURVEY\&quot;,\r\n            \&quot;limitMode\&quot;: \&quot;NA\&quot;\r\n        }\r\n    ]\r\n}&quot;,
  &quot;contentType&quot;: &quot;text/plain&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <katalonVersion>8.6.8</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${Staging FMC}program</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.Staging FMC</defaultValue>
      <description></description>
      <id>4a0150a5-60e2-43e0-a35d-348e83c7fb02</id>
      <masked>false</masked>
      <name>Staging FMC</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
