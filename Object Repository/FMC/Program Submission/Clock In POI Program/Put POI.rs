<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Put POI</name>
   <tag></tag>
   <elementGuidId>77410bf2-07f0-41ec-a2c4-f93bf077553d</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\r\n    \&quot;programType\&quot;: \&quot;CLOCK_IN_POI\&quot;,\r\n    \&quot;programName\&quot;: \&quot;musashi POI\&quot;,\r\n    \&quot;startDate\&quot;: \&quot;2024-01-19T00:00:00\&quot;,\r\n    \&quot;endDate\&quot;: \&quot;2024-01-31T23:59:59\&quot;,\r\n    \&quot;subChannelId\&quot;: \&quot;4072b3ce-341f-4a42-a28f-f86937e31ff6\&quot;,\r\n    \&quot;nationality\&quot;: \&quot;ALL\&quot;,\r\n    \&quot;listMode\&quot;: \&quot;BLACKLIST\&quot;,\r\n    \&quot;sfSegmentUploadId\&quot;: \&quot;2c9380828d1d3181018d1d6092520002\&quot;,\r\n    \&quot;uncheckPartnerIds\&quot;: [\r\n        \&quot;partner10\&quot;,\r\n        \&quot;partner3\&quot;,\r\n        \&quot;partner8\&quot;\r\n    ],\r\n    \&quot;fmcIncentiveRuleDetailRequests\&quot;: [\r\n        {\r\n            \&quot;programType\&quot;: \&quot;CLOCK_IN_POI\&quot;,\r\n            \&quot;bucketId\&quot;: \&quot;2c9380828d07680a018d07f0d6b50003\&quot;,\r\n            \&quot;transactionType\&quot;: \&quot;FIX\&quot;,\r\n            \&quot;amount\&quot;: 15000,\r\n            \&quot;baseOn\&quot;: \&quot;POI_LOCATION\&quot;,\r\n            \&quot;useClockInRule\&quot;: true,\r\n            \&quot;clockInRule\&quot;: \&quot;DAILY\&quot;,\r\n            \&quot;allowedClockInNumber\&quot;: 5\r\n        }\r\n    ]\r\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>accept</name>
      <type>Main</type>
      <value>*/*</value>
      <webElementGuid>4dd50b23-7cfa-4cd2-904e-b7f9c7c66c8e</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>9260de75-3110-4727-a52a-fcdd1bb16196</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.8</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>${Staging FMC}program/CIP2024011823213346003660</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.Staging FMC</defaultValue>
      <description></description>
      <id>b4621902-025c-49ea-a13f-ffb8079d886c</id>
      <masked>false</masked>
      <name>Staging FMC</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
