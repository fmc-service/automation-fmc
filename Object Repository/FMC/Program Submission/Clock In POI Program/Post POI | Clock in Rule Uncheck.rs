<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Post POI | Clock in Rule Uncheck</name>
   <tag></tag>
   <elementGuidId>55c4ca5d-b9ce-43a6-96b1-0856595b4e7f</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\r\n    \&quot;programType\&quot;: \&quot;CLOCK_IN_POI\&quot;,\r\n    \&quot;programName\&quot;: \&quot;Test Post 99\&quot;,\r\n    \&quot;startDate\&quot;: \&quot;2024-12-02T13:20:56\&quot;,\r\n    \&quot;endDate\&quot;: \&quot;2024-12-31T13:20:56\&quot;,\r\n    \&quot;channelId\&quot;: \&quot;155\&quot;,\r\n    \&quot;nationality\&quot;: \&quot;ALL\&quot;,\r\n    \&quot;listMode\&quot;: \&quot;WHITELIST\&quot;,\r\n    \&quot;sfSegmentUploadId\&quot;: \&quot;2c9380828bec5924018bec73afcd0000\&quot;,\r\n    \&quot;uncheckPartnerIds\&quot;: [],\r\n    \&quot;fmcIncentiveRuleDetailRequests\&quot;: [\r\n        {\r\n            \&quot;programType\&quot;: \&quot;CLOCK_IN_POI\&quot;,\r\n            \&quot;bucketId\&quot;: \&quot;2c9380828b80eddb018b848d4f890006\&quot;,\r\n            \&quot;transactionType\&quot;: \&quot;FIX\&quot;,\r\n            \&quot;amount\&quot;: 99,\r\n            \&quot;baseOn\&quot;: \&quot;POI_LOCATION\&quot;,\r\n            \&quot;useClockInRule\&quot;: false,\r\n            \&quot;clockInRule\&quot;: null,\r\n            \&quot;allowedClockInNumber\&quot;: null\r\n        }\r\n    ]\r\n}&quot;,
  &quot;contentType&quot;: &quot;text/plain&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <katalonVersion>8.6.8</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${Staging}program</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.Staging</defaultValue>
      <description></description>
      <id>c3c52e3f-daaf-4385-b698-0dc7fb5a5a8a</id>
      <masked>false</masked>
      <name>Staging</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
