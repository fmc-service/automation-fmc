<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Post POI | Clock in Rule Check Active</name>
   <tag></tag>
   <elementGuidId>68fcba57-35bc-4679-9f69-64d28be0cec5</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\r\n    \&quot;programType\&quot;: \&quot;CLOCK_IN_POI\&quot;,\r\n    \&quot;programName\&quot;: \&quot;Test Post 99\&quot;,\r\n    \&quot;startDate\&quot;: \&quot;2024-12-02T13:20:56\&quot;,\r\n    \&quot;endDate\&quot;: \&quot;2024-12-31T13:20:56\&quot;,\r\n    \&quot;channelId\&quot;: \&quot;155\&quot;,\r\n    \&quot;nationality\&quot;: \&quot;ALL\&quot;,\r\n    \&quot;listMode\&quot;: \&quot;WHITELIST\&quot;,\r\n    \&quot;sfSegmentUploadId\&quot;: \&quot;2c9380828bec5924018bec89592d0002\&quot;,\r\n    \&quot;uncheckPartnerIds\&quot;: [],\r\n    \&quot;fmcIncentiveRuleDetailRequests\&quot;: [\r\n        {\r\n            \&quot;programType\&quot;: \&quot;CLOCK_IN_POI\&quot;,\r\n            \&quot;bucketId\&quot;: \&quot;2c9380828b80eddb018b848d4f890006\&quot;,\r\n            \&quot;transactionType\&quot;: \&quot;FIX\&quot;,\r\n            \&quot;amount\&quot;: 99,\r\n            \&quot;baseOn\&quot;: \&quot;POI_LOCATION\&quot;,\r\n            \&quot;useClockInRule\&quot;: true,\r\n            \&quot;clockInRule\&quot;: \&quot;DAILY\&quot;,\r\n            \&quot;allowedClockInNumber\&quot;: 2\r\n        }\r\n    ]\r\n}&quot;,
  &quot;contentType&quot;: &quot;text/plain&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <katalonVersion>8.6.8</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${Staging}program</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.Staging</defaultValue>
      <description></description>
      <id>fa4a9e1b-20ee-4f3f-bff0-a6999b9fa468</id>
      <masked>false</masked>
      <name>Staging</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
