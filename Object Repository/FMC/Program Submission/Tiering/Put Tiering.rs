<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Put Tiering</name>
   <tag></tag>
   <elementGuidId>8875e0b0-e408-4d82-9f94-dd32c7d2a82a</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\r\n  \&quot;programType\&quot;: \&quot;PUT_IN_SERVICE_TIERING\&quot;,\r\n  \&quot;programName\&quot;: \&quot;Test Put Tiering\&quot;,\r\n  \&quot;startDate\&quot;: \&quot;2024-12-02T13:20:56\&quot;,\r\n  \&quot;endDate\&quot;: \&quot;2024-12-31T13:20:56\&quot;,\r\n  \&quot;channelId\&quot;: \&quot;155\&quot;,\r\n  \&quot;nationality\&quot;: \&quot;ALL\&quot;,\r\n  \&quot;area\&quot;: \&quot;Area 1\&quot;,\r\n  \&quot;region\&quot;: \&quot;Region 1\&quot;,\r\n  \&quot;branch\&quot;: \&quot;Branch 1\&quot;,\r\n  \&quot;cluster\&quot;: \&quot;Cluster 1\&quot;,\r\n  \&quot;listMode\&quot;: \&quot;NA\&quot;,\r\n  \&quot;sfSegmentUploadId\&quot; : \&quot;2c9380828bb89dc6018bc139c7180000\&quot;,\r\n  \&quot;uncheckPartnerIds\&quot;: [],\r\n  \&quot;fmcIncentiveRuleDetailRequests\&quot;: [\r\n    {\r\n      \&quot;programType\&quot;: \&quot;PUT_IN_SERVICE_TIERING\&quot;,\r\n      \&quot;bucketId\&quot;: \&quot;2c9380828b80eddb018b848d4f890006\&quot;,\r\n      \&quot;transactionType\&quot;: \&quot;Koin\&quot;,\r\n      \&quot;amount\&quot;: 99,\r\n      \&quot;baseOn\&quot;: \&quot;TRANSACTION\&quot;,\r\n      \&quot;minValue\&quot;: 9,\r\n      \&quot;maxValue\&quot;: 15,\r\n      \&quot;additionalFeeType\&quot;: \&quot;FIX\&quot;,\r\n      \&quot;additionalFeeAmount\&quot;: 10\r\n    },\r\n    {\r\n      \&quot;programType\&quot;: \&quot;PUT_IN_SERVICE_TIERING\&quot;,\r\n      \&quot;bucketId\&quot;: \&quot;2c9380828b80eddb018b848d4f890006\&quot;,\r\n      \&quot;transactionType\&quot;: \&quot;Koin\&quot;,\r\n      \&quot;amount\&quot;: 99,\r\n      \&quot;baseOn\&quot;: \&quot;BANDWIDTH\&quot;,\r\n      \&quot;minValue\&quot;: 10,\r\n      \&quot;maxValue\&quot;: 20\r\n    },\r\n    {\r\n      \&quot;programType\&quot;: \&quot;PUT_IN_SERVICE_TIERING\&quot;,\r\n      \&quot;bucketId\&quot;: \&quot;2c9380828b80eddb018b848d4f890006\&quot;,\r\n      \&quot;transactionType\&quot;: \&quot;Koin\&quot;,\r\n      \&quot;amount\&quot;: 99,\r\n      \&quot;baseOn\&quot;: \&quot;BANDWIDTH\&quot;,\r\n      \&quot;minValue\&quot;: 21,\r\n      \&quot;maxValue\&quot;: 50\r\n      }\r\n  ]\r\n}\r\n&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>accept</name>
      <type>Main</type>
      <value>*/*</value>
      <webElementGuid>fbe1e286-b57d-440b-975b-afb8660d27e4</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>d1a21fb4-79ec-43ee-87da-759dd8f8cf46</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.8</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>${Staging FMC}program/2c9380828bcc7234018bcc9f7e850013</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.Staging FMC</defaultValue>
      <description></description>
      <id>ea5112a6-0152-483d-9a95-8c3f1bbb60f5</id>
      <masked>false</masked>
      <name>Staging FMC</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
