<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Post Tiering</name>
   <tag></tag>
   <elementGuidId>2c2530f4-5687-4ebc-a1de-86a3f5381034</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\r\n    \&quot;programType\&quot;: \&quot;PUT_IN_SERVICE_TIERING\&quot;,\r\n    \&quot;programName\&quot;: \&quot;Program Test Vasda 99\&quot;,\r\n    \&quot;startDate\&quot;: \&quot;2028-12-02T13:20:56\&quot;,\r\n    \&quot;endDate\&quot;: \&quot;2029-12-31T13:20:56\&quot;,\r\n    \&quot;subChannelId\&quot;: \&quot;4072b3ce-341f-4a42-a28f-f86937e31ff6\&quot;,\r\n    \&quot;nationality\&quot;: \&quot;ALL\&quot;,\r\n    \&quot;listMode\&quot;: \&quot;WHITELIST\&quot;,\r\n    \&quot;sfSegmentUploadId\&quot;: \&quot;2c9380828c824067018c85e9e1d90013\&quot;,\r\n    \&quot;uncheckPartnerIds\&quot;: [],\r\n    \&quot;fmcIncentiveRuleDetailRequests\&quot;: [\r\n        {\r\n            \&quot;programType\&quot;: \&quot;PUT_IN_SERVICE_TIERING\&quot;,\r\n            \&quot;bucketId\&quot;: \&quot;2c9380828b80eddb018b848d4f890006\&quot;,\r\n            \&quot;transactionType\&quot;: \&quot;FIX\&quot;,\r\n            \&quot;amount\&quot;: 100,\r\n            \&quot;baseOn\&quot;: \&quot;TRANSACTION\&quot;,\r\n            \&quot;minValue\&quot;: 1,\r\n            \&quot;maxValue\&quot;: 20,\r\n            \&quot;additionalFeeType\&quot;: \&quot;FIX\&quot;,\r\n            \&quot;additionalFeeAmount\&quot;: 50\r\n        },\r\n        {\r\n            \&quot;programType\&quot;: \&quot;PUT_IN_SERVICE_TIERING\&quot;,\r\n            \&quot;bucketId\&quot;: \&quot;2c9380828b80eddb018b848d4f890006\&quot;,\r\n            \&quot;transactionType\&quot;: \&quot;FIX\&quot;,\r\n            \&quot;amount\&quot;: 100,\r\n            \&quot;baseOn\&quot;: \&quot;BANDWIDTH\&quot;,\r\n            \&quot;minValue\&quot;: 30,\r\n            \&quot;maxValue\&quot;: 49\r\n        },\r\n        {\r\n            \&quot;programType\&quot;: \&quot;PUT_IN_SERVICE_TIERING\&quot;,\r\n            \&quot;bucketId\&quot;: \&quot;2c9380828b80eddb018b848d4f890006\&quot;,\r\n            \&quot;transactionType\&quot;: \&quot;FIX\&quot;,\r\n            \&quot;amount\&quot;: 150,\r\n            \&quot;baseOn\&quot;: \&quot;BANDWIDTH\&quot;,\r\n            \&quot;minValue\&quot;: 50,\r\n            \&quot;maxValue\&quot;: 100\r\n        }\r\n    ]\r\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>accept</name>
      <type>Main</type>
      <value>*/*</value>
      <webElementGuid>5c14c89b-fc11-44da-92ef-1a9c6bd45c12</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>3c7041f4-8edb-4a94-b492-3a547e1c6d3d</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.8</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${Staging FMC}program</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.Staging FMC</defaultValue>
      <description></description>
      <id>7ebd0a16-e0c7-47cd-934c-a1ec764be208</id>
      <masked>false</masked>
      <name>Staging FMC</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
