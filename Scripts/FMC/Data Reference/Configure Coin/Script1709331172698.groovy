import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import java.time.LocalDateTime

def String token = "scheduler-0e60543e-9b87-427b-ad76-cdf8e695f218"
def String forbiddenToken = "2c9a80828de41204018de615a57c00008966b671-cf0c-4c08-94c7-44e13c423a68"
def Integer validYear = 2310
def String validDate = "-12-30"


CustomKeywords.'utils.ExcelReporting.executionTime'('FMC-Configure-Coin')


TC0001(token, validYear, validDate)
validYear+=1
TC0002(token)
validYear+=1
TC0003(validYear, validDate)
validYear+=1
TC0004(token, validYear, validDate)
validYear+=1
TC0005(forbiddenToken, validYear, validDate)
validYear+=1
TC0006(token, validYear, validDate)
validYear+=2
TC0007(token, validYear, validDate)
validYear+=1
TC0008(token, validYear, validDate)
validYear+=1
TC0009(token, forbiddenToken, validYear, validDate)
TC0010(token)
TC0011(token)
TC0012()
TC0013(forbiddenToken)

// Valid Create Request
def TC0001(String authToken, Integer validYear, String validDate ) {
	try {
		GlobalVariable.authToken = authToken
		def year = validYear.toString()
		
		def body = '{ "title": "'+year+validDate+'", "year" :"' +year+'"}'
		def json = new JsonSlurper()
		def payload = json.parseText(body)
		
		def response = CustomKeywords.'configureCoin.Custom.createConfigureCoin'(body)
		def parsed = json.parseText(response.getResponseText())
		assert response.getStatusCode() == 200

		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', response.getResponseText(), 'Create Configure Coin Data', true)
	}
	catch (Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', ex.message, 'Create Configure Coin Data', false)
		
	}
}

// Expiration date and year doesnt match
def TC0002(String token) {
	try {
		GlobalVariable.authToken = token
		def body = '{ "expiryDate": "2137-12-30", "year" : "2138"}'
		
		def response = CustomKeywords.'configureCoin.Custom.createConfigureCoin'(body)
		assert response.getStatusCode() == 400
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', response.getResponseText(), 'Create Configure Coin Data | Invalid Payload', true)
	} catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', ex.message, 'Create Configure Coin Data | Invalid Payload', false)
	}

}

// Create Without Auth Token
def TC0003(Integer validYear, String validDate) {
	try {
		GlobalVariable.authToken = ""
		def targetYear = validYear.toString()
		def body = '{ "expiryDate": "'+targetYear+validDate+'", "year" :"' +targetYear+'"}'
		
		
		def response = CustomKeywords.'configureCoin.Custom.createConfigureCoin'(body)
		assert response.getStatusCode() == 500
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', response.getResponseText(), 'Create Configure Coin Data | No Auth', true)
	} catch (Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', ex.message, 'Create Configure Coin Data | No Auth', false)
	}
	
}

// Create Duplicate Configure Coin
def TC0004(String token, Integer validYear, String validDate) {
	try {
		GlobalVariable.authToken = token
		def targetYear = validYear.toString()
		def body = '{ "expiryDate": "'+targetYear+validDate+'", "year" :"' +targetYear+'"}'
		
		def response = CustomKeywords.'configureCoin.Custom.createConfigureCoin'(body)
		assert response.getStatusCode() == 200
		
				
		def response2 = CustomKeywords.'configureCoin.Custom.createConfigureCoin'(body)
		assert response2.getStatusCode() == 400
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', response.getResponseText(), 'Create Existing Configure Coin', true)
	}
	catch (Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', ex.message, 'Create Existing Configure Coin', false)
	}
}


// Create Order permission denied
def TC0005(String forbiddenToken, Integer validYear, String validDate) {
	try {
		GlobalVariable.authToken = forbiddenToken
		def targetYear = validYear.toString()
		def body = '{ "expiryDate": "'+targetYear+validDate+'", "year" :"' +targetYear+'"}'
				
		def response = CustomKeywords.'configureCoin.Custom.createConfigureCoin'(body)
		assert response.getStatusCode() == 500
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', response.getResponseText(), 'Create Configure Coin Forbidden', true)
	}
	catch (Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', ex.message, 'Create Configure Coin Forbidden', false)
	}
}

// Edit Valid Order
def TC0006(String token, Integer validYear, String validDate) {
	try {
		def jsonSlurper = new JsonSlurper()
		GlobalVariable.authToken = token
		def targetYear = validYear.toString()
		def body = '{ "expiryDate": "'+targetYear+validDate+'", "year" :"' +targetYear+'"}'
				
		def response = CustomKeywords.'configureCoin.Custom.createConfigureCoin'(body)
		assert response.getStatusCode() == 200
		
		validYear+=1
		def targetYear2 = validYear.toString()
		def id = jsonSlurper.parseText(response.getResponseText()).data
		def body2 = '{ "expiryDate": "'+targetYear2+validDate+'", "year" :"' +targetYear2+'"}'
		
		def editResponse = CustomKeywords.'configureCoin.Custom.editConfigureCoin'(body2, id)
		assert editResponse.getStatusCode() == 200
		
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', editResponse.getResponseText(), 'Edit Configure Coin', true)
	}
	catch (Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', ex.message, 'Edit Configure Coin', false)
	}
}

// Edit Duplicate Configure Coin
def TC0007(String token,Integer validYear, String validDate) {
	try {
		GlobalVariable.authToken = token
		def jsonSlurper = new JsonSlurper()
		
		def list = CustomKeywords.'configureCoin.Custom.getConfigureCoinList'("-expiryDate", "1", "1")
		assert list.getStatusCode() == 200
		list = jsonSlurper.parseText(list.getResponseText())
		
		def existingData = list.data.content[0]
		
		def targetYear = validYear.toString()
		def body = '{ "expiryDate": "'+targetYear+validDate+'", "year" :"' +targetYear+'"}'
				
		def response = CustomKeywords.'configureCoin.Custom.createConfigureCoin'(body)
		assert response.getStatusCode() == 200
	
		def id = jsonSlurper.parseText(response.getResponseText()).data
		def body2 = '{ "expiryDate": "'+existingData.expiryDate+'", "year" :"' +existingData.year+'"}'
		
		def editResponse = CustomKeywords.'configureCoin.Custom.editConfigureCoin'(body2, id)
		assert editResponse.getStatusCode() == 400
		
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', editResponse.getResponseText(), 'Edit Configure Coin Duplicate Data', true)
	}
	catch (Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', ex.message, 'Edit Configure Coin Duplicate Data', false)
	}
}

// Edit Configure Coin Unauthorize
def TC0008(String token,Integer validYear, String validDate) {
	try {
		def jsonSlurper = new JsonSlurper()
		GlobalVariable.authToken = token
		def targetYear = validYear.toString()
		def body = '{ "expiryDate": "'+targetYear+validDate+'", "year" :"' +targetYear+'"}'
				
		def response = CustomKeywords.'configureCoin.Custom.createConfigureCoin'(body)
		assert response.getStatusCode() == 200
		
		GlobalVariable.authToken = ''
		validYear+=1
		def targetYear2 = validYear.toString()
		def id = jsonSlurper.parseText(response.getResponseText()).data
		def body2 = '{ "expiryDate": "'+targetYear2+validDate+'", "year" :"' +targetYear2+'"}'
		
		def editResponse = CustomKeywords.'configureCoin.Custom.editConfigureCoin'(body2, id)
		assert editResponse.getStatusCode() == 500
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', editResponse.getResponseText(), 'Edit Configure Coin No Auth', true)
		
	}
	catch (Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', ex.message, 'Edit Configure Coin No Auth', false)
	}
}


// Edit Configure Coin Forbidden
def TC0009(String token, String forbiddenToken,Integer validYear, String validDate) {
	try {
		def jsonSlurper = new JsonSlurper()
		GlobalVariable.authToken = token
		def targetYear = validYear.toString()
		def body = '{ "expiryDate": "'+targetYear+validDate+'", "year" :"' +targetYear+'"}'
				
		def response = CustomKeywords.'configureCoin.Custom.createConfigureCoin'(body)
		assert response.getStatusCode() == 200
		
		GlobalVariable.authToken = forbiddenToken
		validYear+=1
		def targetYear2 = validYear.toString()
		def id = jsonSlurper.parseText(response.getResponseText()).data
		def body2 = '{ "expiryDate": "'+targetYear+validDate+'", "year" :"' +targetYear+'"}'
		
		def editResponse = CustomKeywords.'configureCoin.Custom.editConfigureCoin'(body2, id)
		assert editResponse.getStatusCode() == 500
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', editResponse.getResponseText(), 'Edit Configure Coin Forbidden', true)
	}
	catch (Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', ex.message, 'Edit Configure Coin Forbidden', false)
	}
}

// Test page size and pagination
def TC0010(String token) {
	try {
		def jsonSlurper = new JsonSlurper()
		GlobalVariable.authToken = token
		
		def response = CustomKeywords.'configureCoin.Custom.getConfigureCoinList'("",'1','25')
		assert response.getStatusCode() == 200
		def jsonResponse = jsonSlurper.parseText(response.getResponseText())
		
		assert jsonResponse.data.content.size() == 25
		def firstContent = jsonResponse.data.content[0]
		
		def response2 = CustomKeywords.'configureCoin.Custom.getConfigureCoinList'("",'2','3')
		assert response2.getStatusCode() == 200
		def jsonResponse2 = jsonSlurper.parseText(response2.getResponseText())
		
		assert jsonResponse2.data.content.size() == 3
		assert firstContent.coinId != jsonResponse2.data.content[0].coinId
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', response2.getResponseText(), 'Configure Coin List Page Size And Page Index', true)
	} catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', ex.message, 'Configure Coin List Page Size And Page Index', false)
	}
}

// Test Sort 
def TC0011(String token) {
	try {
		GlobalVariable.authToken = token
		def df = "yyyy-MM-dd HH:mm:ss"
		def jsonSlurper = new JsonSlurper()
		
		def response = CustomKeywords.'configureCoin.Custom.getConfigureCoinList'("expiryDate",'1','2')
		assert response.getStatusCode() == 200
		def jsonResponse = jsonSlurper.parseText(response.getResponseText())
				
		assert LocalDateTime.parse(jsonResponse.data.content[1].expiryDate).isAfter(LocalDateTime.parse(jsonResponse.data.content[0].expiryDate))
				
		def response2 = CustomKeywords.'configureCoin.Custom.getConfigureCoinList'("-expiryDate",'1','2')
		assert response2.getStatusCode() == 200
		def jsonResponse2 = jsonSlurper.parseText(response2.getResponseText())
		
		assert LocalDateTime.parse(jsonResponse2.data.content[0].expiryDate).isAfter(LocalDateTime.parse(jsonResponse2.data.content[1].expiryDate))
				
		def response3 = CustomKeywords.'configureCoin.Custom.getConfigureCoinList'("year",'1','2')
		assert response3.getStatusCode() == 200
		def jsonResponse3 = jsonSlurper.parseText(response3.getResponseText())
		
		assert LocalDateTime.parse(jsonResponse3.data.content[1].expiryDate).isAfter(LocalDateTime.parse(jsonResponse3.data.content[0].expiryDate))
				
		def response4 = CustomKeywords.'configureCoin.Custom.getConfigureCoinList'("-year",'1','2')
		assert response4.getStatusCode() == 200
		def jsonResponse4 = jsonSlurper.parseText(response4.getResponseText())
		
		assert LocalDateTime.parse(jsonResponse4.data.content[0].expiryDate).isAfter(LocalDateTime.parse(jsonResponse4.data.content[1].expiryDate))	
		
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', response4.getResponseText(), 'Configure Coin List Sorting', true)
	}
	catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', ex.message, 'Configure Coin List Sorting', false)
	}
}

// Test Unauthorize Token Get list
def TC0012() {
	try{
		GlobalVariable.authToken = ''
		def response = CustomKeywords.'configureCoin.Custom.getConfigureCoinList'("expiryDate",'1','2')
		assert response.getStatusCode() == 500
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', response.getResponseText(), 'Configure Coin List No Auth', true)
	}catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', ex.message, 'Configure Coin List No Auth', false)
	}
}

// Test Forbidden Token Get list
def TC0013(String forbiddenToken) {
	try {
		GlobalVariable.authToken = forbiddenToken
		def response = CustomKeywords.'configureCoin.Custom.getConfigureCoinList'("expiryDate",'1','2')
		assert response.getStatusCode() == 500
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', response.getResponseText(), 'Configure Coin List Forbidden', true)
	}catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Configure-Coin', ex.message, 'Configure Coin List Forbidden', false)
	}
}