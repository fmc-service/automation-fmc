import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import java.time.LocalDateTime

def String token = "scheduler-0e60543e-9b87-427b-ad76-cdf8e695f218"
def String forbiddenToken = "2c9a80828de41204018de615a57c00008966b671-cf0c-4c08-94c7-44e13c423a68"
def String validSurveyId = "SURVEY1"

CustomKeywords.'utils.ExcelReporting.executionTime'('FMC-Survey-Reference')

TC0001(token)
TC0002(token)
TC0003()
TC0004(forbiddenToken)
TC0005(token, validSurveyId)
TC0006(token)
TC0007(token, validSurveyId)
TC0008(forbiddenToken)
TC0009()

// Test page size and pagination
def TC0001(String token) {
	try {
		def jsonSlurper = new JsonSlurper()
		GlobalVariable.authToken = token
		
		def response = CustomKeywords.'surveyReference.Custom.getSurveyList'("",'1','2')
		assert response.getStatusCode() == 200
		def jsonResponse = jsonSlurper.parseText(response.getResponseText())
		if(jsonResponse.data.content.size == 0) {
			throw new Exception('Survey is empty')
		}
		
		assert jsonResponse.data.content.size() == 25
		def firstContent = jsonResponse.data.content[0]
		
		def response2 = CustomKeywords.'surveyReference.Custom.getSurveyList'("",'2','3')
		assert response2.getStatusCode() == 200
		def jsonResponse2 = jsonSlurper.parseText(response2.getResponseText())
		
		assert jsonResponse2.data.content.size() == 3
		assert firstContent.coinId != jsonResponse2.data.content[0].coinId
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Survey-Reference', response2.getResponseText(), 'Survey Reference List Page Size And Page Index', true)
	} catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Survey-Reference', ex.message, 'Survey Reference List Page Size And Page Index', false)
	}
}

// Test Sort 
def TC0002(String token) {
	try {
		GlobalVariable.authToken = token
		def df = "yyyy-MM-dd HH:mm:ss"
		def jsonSlurper = new JsonSlurper()
		
		def response = CustomKeywords.'surveyReference.Custom.getSurveyList'("endDate",'1','2')
		assert response.getStatusCode() == 200
		def jsonResponse = jsonSlurper.parseText(response.getResponseText())
		if(jsonResponse.data.content.size == 0) {
			throw new Exception('Survey is empty')
		}
				
		assert LocalDateTime.parse(jsonResponse.data.content[1].endDate).isAfter(LocalDateTime.parse(jsonResponse.data.content[0].endDate))
				
		def response2 = CustomKeywords.'surveyReference.Custom.getSurveyList'("-endDate",'1','2')
		assert response2.getStatusCode() == 200
		def jsonResponse2 = jsonSlurper.parseText(response2.getResponseText())
		
		assert LocalDateTime.parse(jsonResponse2.data.content[0].endDate).isAfter(LocalDateTime.parse(jsonResponse2.data.content[1].endDate))
				
		def response3 = CustomKeywords.'surveyReference.Custom.getSurveyList'("startDate",'1','2')
		assert response3.getStatusCode() == 200
		def jsonResponse3 = jsonSlurper.parseText(response3.getResponseText())
		
		assert LocalDateTime.parse(jsonResponse3.data.content[1].endDate).isAfter(LocalDateTime.parse(jsonResponse3.data.content[0].endDate))
				
		def response4 = CustomKeywords.'surveyReference.Custom.getSurveyList'("-startDate",'1','2')
		assert response4.getStatusCode() == 200
		def jsonResponse4 = jsonSlurper.parseText(response4.getResponseText())
		
		assert LocalDateTime.parse(jsonResponse4.data.content[0].endDate).isAfter(LocalDateTime.parse(jsonResponse4.data.content[1].endDate))	
		
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Survey-Reference', response4.getResponseText(), 'Survey Reference List Sorting', true)
	}
	catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Survey-Reference', ex.message, 'Survey Reference List Sorting', false)
	}
}

// Test Unauthorize Token Get list
def TC0003() {
	try{
		GlobalVariable.authToken = ''
		def response = CustomKeywords.'surveyReference.Custom.getSurveyList'("",'1','2')
		assert response.getStatusCode() == 500
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Survey-Reference', response.getResponseText(), 'Survey Reference List No Auth', true)
	}catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Survey-Reference', ex.message, 'Survey Reference List No Auth', false)
	}
}

// Test Forbidden Token Get list
def TC0004(String forbiddenToken) {
	try {
		GlobalVariable.authToken = forbiddenToken
		def response = CustomKeywords.'surveyReference.Custom.getSurveyList'("",'1','2')
		assert response.getStatusCode() == 500
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Survey-Reference', response.getResponseText(), 'Survey Reference List Forbidden', true)
	}catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Survey-Reference', ex.message, 'Survey Reference List Forbidden', false)
	}
}

// Test Filter Survey ID
def TC0005(String token, String validSurveyId) {
	try {
		def jsonSlurper = new JsonSlurper()
		
		GlobalVariable.authToken = token
		def response = CustomKeywords.'surveyReference.Custom.getSurveyList'("",'1','2',validSurveyId)
		assert response.getStatusCode() == 200
		def list = jsonSlurper.parseText(response.getResponseText())
		if(list.data.content.size == 0) {
			throw new Exception('Survey is empty')
		}
		
		for (survey in list.data.content) {
			assert survey.surveyId == validSurveyId
		}
		
		
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Survey-Reference', response.getResponseText(), 'Survey Reference List Filter Survey Id', true)
		
	}catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Survey-Reference', ex.message, 'Survey Reference List Filter Survey Id', false)
		
	}
}

// Test Download Header
def TC0006(String token) {
	try {		
		GlobalVariable.authToken = token
		def response = CustomKeywords.'surveyReference.Custom.getDownloadList'()
		assert response.getStatusCode() == 200
		def csv = response.getResponseText()
		
		def len = csv.split('\n')

		def header = csv
		if(len.size() > 1) {
			header = csv.split('\n')[0]
		}
		
		assert header.contains('surveyId')
		assert header.contains('surveyTitle')
		assert header.contains('startDate')
		assert header.contains('endDate')
		
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Survey-Reference', csv, 'Download Survey', true)
		
	}catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Survey-Reference', ex.message, 'Download Survey', false)
	}
}

// Test Download Filter Survey Id
def TC0007(String token, String validSurveyId) {
	try {		
		GlobalVariable.authToken = token
		def response = CustomKeywords.'surveyReference.Custom.getDownloadList'('', validSurveyId)
		assert response.getStatusCode() == 200
		def csv = response.getResponseText()
		def len = csv.split('\n')
		
		if(len.size() == 1) {
			throw new Exception("Empty Survey List")
		}
		
		for(survey in csv.split('\n')) {
			def surveyId = survey.split(',')[0]
			if(surveyId == 'surveyId') {
				continue
			}
			assert surveyId == validSurveyId
		}
		
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Survey-Reference', csv, 'Download Survey Filter Survey Id', true)
		
	}catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Survey-Reference', ex.message, 'Download Survey Filter Survey Id', false)
	}
}

// Test Download Forbidden
def TC0008(String forbiddenToken) {
	try {		
		GlobalVariable.authToken = forbiddenToken
		def response = CustomKeywords.'surveyReference.Custom.getDownloadList'()
		assert response.getStatusCode() == 500
		
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Survey-Reference', response.getResponseText(), 'Download Survey Forbidden', true)
		
	}catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Survey-Reference', ex.message, 'Download Survey Forbidden', false)
	}
}

// Test Download No Auth
def TC0009() {
	try {		
		GlobalVariable.authToken = ''
		def response = CustomKeywords.'surveyReference.Custom.getDownloadList'()
		assert response.getStatusCode() == 500
		
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Survey-Reference', response.getResponseText(), 'Download Survey No auth', true)
		
	}catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Survey-Reference', ex.message, 'Download Survey No auth', false)
	}
}