import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import java.time.LocalDateTime

def String token = "scheduler-0e60543e-9b87-427b-ad76-cdf8e695f218"
def String forbiddenToken = "2c9a80828de41204018de615a57c00008966b671-cf0c-4c08-94c7-44e13c423a68"

CustomKeywords.'utils.ExcelReporting.executionTime'('FMC-Report')

TC0001(token)
TC0002()
TC0003(forbiddenToken)
TC0004(token)
TC0005()
TC0006(forbiddenToken)
TC0007(token)
TC0008()
TC0009(forbiddenToken)
TC0010(token)
TC0011()
TC0012(forbiddenToken)


// Test Get Total Creation Coin
def TC0001(String token) {
	try {
		def jsonSlurper = new JsonSlurper()
		GlobalVariable.authToken = token
		
		def response = CustomKeywords.'report.Custom.getTotalKoinCreation'()
		assert response.getStatusCode() == 200
		
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', response.getResponseText(), 'Report Total Coin Created', true)
	} catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', ex.message, 'Report Total Coin Created', false)
	}
}

// Test Unauthorize Token Get list
def TC0002() {
	try{
		GlobalVariable.authToken = ''
		def response = CustomKeywords.'report.Custom.getTotalKoinCreation'()
		assert response.getStatusCode() == 500
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', response.getResponseText(), 'Report Total Coin Created No Auth', true)
	}catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', ex.message, 'Report Total Coin Created No Auth', false)
	}
}

// Test Forbidden Token Get list
def TC0003(String forbiddenToken) {
	try {
		GlobalVariable.authToken = forbiddenToken
		def response = CustomKeywords.'report.Custom.getTotalKoinCreation'()
		assert response.getStatusCode() == 500
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', response.getResponseText(), 'Report Total Coin Created Forbidden', true)
	}catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', ex.message, 'Report Total Coin Created Forbidden', false)
	}
}

// Test Get Total Koin Redemption
def TC0004(String token) {
	try {
		def jsonSlurper = new JsonSlurper()
		GlobalVariable.authToken = token
		
		def response = CustomKeywords.'report.Custom.getTotalKoinRedemtion'()
		assert response.getStatusCode() == 200
		
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', response.getResponseText(), 'Report Total Coin Redemtion', true)
	} catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', ex.message, 'Report Total Coin Redemtion', false)
	}
}

// Test Unauthorize Token Get list
def TC0005() {
	try{
		GlobalVariable.authToken = ''
		def response = CustomKeywords.'report.Custom.getTotalKoinRedemtion'()
		assert response.getStatusCode() == 500
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', response.getResponseText(), 'Report Total Coin Redemtion No Auth', true)
	}catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', ex.message, 'Report Total Coin Redemtion No Auth', false)
	}
}

// Test Forbidden Token Get list
def TC0006(String forbiddenToken) {
	try {
		GlobalVariable.authToken = forbiddenToken
		def response = CustomKeywords.'report.Custom.getTotalKoinRedemtion'()
		assert response.getStatusCode() == 500
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', response.getResponseText(), 'Report Total Coin Redemtion Forbidden', true)
	}catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', ex.message, 'Report Total Coin Redemtion Forbidden', false)
	}
}

// Test Get Top ten sales
def TC0007(String token) {
	try {
		def jsonSlurper = new JsonSlurper()
		GlobalVariable.authToken = token
		
		def response = CustomKeywords.'report.Custom.getTopTenSales'()
		assert response.getStatusCode() == 200
		
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', response.getResponseText(), 'Report Top Ten Sales Force', true)
	} catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', ex.message, 'Report Top Ten Sales Force', false)
	}
}

// Test Unauthorize Token Get list
def TC0008() {
	try{
		GlobalVariable.authToken = ''
		def response = CustomKeywords.'report.Custom.getTopTenSales'()
		assert response.getStatusCode() == 500
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', response.getResponseText(), 'Report Top Ten Sales Force No Auth', true)
	}catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', ex.message, 'Report Top Ten Sales Force No Auth', false)
	}
}

// Test Forbidden Token Get list
def TC0009(String forbiddenToken) {
	try {
		GlobalVariable.authToken = forbiddenToken
		def response = CustomKeywords.'report.Custom.getTopTenSales'()
		assert response.getStatusCode() == 500
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', response.getResponseText(), 'Report Top Ten Sales Force Forbidden', true)
	}catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', ex.message, 'Report Top Ten Sales Force Forbidden', false)
	}
}

// Test Get Achivement
def TC0010(String token) {
	try {
		def jsonSlurper = new JsonSlurper()
		GlobalVariable.authToken = token
		
		def response = CustomKeywords.'report.Custom.getSalesForceBWAchivement'()
		assert response.getStatusCode() == 200
		
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', response.getResponseText(), 'Report Achivement', true)
	} catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', ex.message, 'Report Achivement', false)
	}
}

// Test Unauthorize Token Get list
def TC0011() {
	try{
		GlobalVariable.authToken = ''
		def response = CustomKeywords.'report.Custom.getSalesForceBWAchivement'()
		assert response.getStatusCode() == 500
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', response.getResponseText(), 'Report Achivement', true)
	}catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', ex.message, 'Report Achivement', false)
	}
}

// Test Forbidden Token Get list
def TC0012(String forbiddenToken) {
	try {
		GlobalVariable.authToken = forbiddenToken
		def response = CustomKeywords.'report.Custom.getSalesForceBWAchivement'()
		assert response.getStatusCode() == 500
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', response.getResponseText(), 'Report Achivement', true)
	}catch(Exception | AssertionError ex) {
		CustomKeywords.'utils.ExcelReporting.excelReporting'('FMC-Report', ex.message, 'Report Achivement', false)
	}
}

