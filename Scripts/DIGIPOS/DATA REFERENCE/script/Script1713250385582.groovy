import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

TC001()

def TC001() {
	try {
		WebUI.callTestCase(findTestCase('Test Cases/AUTH/superadmin'), [:], FailureHandling.STOP_ON_FAILURE)
				
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Data Reference'))
		
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Sales Force Reference'))
		
		WebUI.verifyElementVisible(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/h3_Data Reference'))
		
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_SF ID'))
		WebUI.delay(2)
		'SORT ASC SF ID'
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_SF ID'))
		WebUI.delay(2)
		'SORT DESC SF ID'
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_SF ID'))
		
		
		
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_SF Code'))
		WebUI.delay(2)
		'SORT ASC SF Code'
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_SF Code'))
		WebUI.delay(2)
		'SORT DESC SF Code'
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_SF Code'))
		
		
		
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Nama'))
		WebUI.delay(2)
		'SORT ASC Nama'
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Nama'))
		WebUI.delay(2)
		'SORT DESC Nama'
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Nama'))
		
		
		
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Area Name'))
		WebUI.delay(2)
		'SORT ASC Area Name'
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Area Name'))
		WebUI.delay(2)
		'SORT DESC Area Name'
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Area Name'))
		
		
		
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Regional'))
		WebUI.delay(2)
		'SORT ASC Regional'
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Regional'))
		WebUI.delay(2)
		'SORT DESC Regional'
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Regional'))
		
		
		WebUI.scrollToElement(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Kabupaten'), 150)
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Branch'))
		WebUI.delay(2)
		'SORT ASC Branch'
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Branch'))
		WebUI.delay(2)
		'SORT DESC Branch'
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Branch'))
		
		
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Cluster'))
		WebUI.delay(2)
		'SORT ASC Cluster'
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Cluster'))
		WebUI.delay(2)
		'SORT DESC Cluster'
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Cluster'))
		
		
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Kabupaten'))
		WebUI.delay(2)
		'SORT ASC Kabupaten'
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Kabupaten'))
		WebUI.delay(2)
		'SORT DESC Kabupaten'
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/span_Kabupaten'))
		
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/button next'))
		WebUI.delay(2)
		WebUI.click(findTestObject('Object Repository/DIGIPOS/DATA REFERENCE/Sales Force Reference/button previous'))
		
		WebUI.comment(' - End')
	}
	catch (Exception ex) {
		println(ex)
	}
}