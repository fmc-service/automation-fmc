package bucketReference

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class Custom {

	@Keyword
	def createBucketReference(requestBody) {
		def response = WS.sendRequestAndVerify(
				findTestObject('Object Repository/FMC/Data Reference/Bucket Reference/Post Tiering Bucket Reference',
				[body: requestBody]))

		return response
	}

	@Keyword
	def getBucketReference(id) {
		def response = WS.sendRequestAndVerify(
				findTestObject('Object Repository/FMC/Data Reference/Bucket Reference/Post Tiering Bucket Reference',
				["id": id]
				))
		return response
	}

	@Keyword
	def editBucketReference(requestBody, id) {
		def response = WS.sendRequestAndVerify(
				findTestObject('Object Repository/FMC/Data Reference/Bucket Reference/Put Bucker Referece - Edit',
				[body: requestBody, "id": id]
				))
		return response
	}
}
