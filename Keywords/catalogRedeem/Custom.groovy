package catalogRedeem

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class requestBody {
	String title
	String description
	String termsAndConditions
	int coinAmount
	String startDate
	String endDate
	String bannerImageName
	String bannerImageBase64
}

public class Custom {

	@Keyword
	def createCatalogRedeem(requestBody) {
		def response = WS.sendRequestAndVerify(
				findTestObject("Object Repository/FMC/Data Reference/Catalog Redeem/Post Finpay | Catalog Redeem",
				[body: requestBody]))

		return response
	}

	@Keyword
	def getCatalogRedeemList(sortBy, page, pageSize) {
		def response = WS.sendRequestAndVerify(
				findTestObject(
				'Object Repository/FMC/Data Reference/Catalog Redeem/Get Catalog Redeem - List',
				["sortBy": sortBy, "page": page, "pageSize": pageSize]
				))
		return response
	}

	@Keyword
	def getCatalogRedeem(id) {
		def response = WS.sendRequestAndVerify(
				findTestObject(
				'Object Repository/FMC/Data Reference/Catalog Redeem/Get Catalog Redeem - Detail',
				["id": id]
				))
		return response
	}

	@Keyword
	def editCatalogRedeem(requestBody, id) {
		def response = WS.sendRequestAndVerify(
				findTestObject("Object Repository/FMC/Data Reference/Catalog Redeem/Put Catalog Redeem",
				[body: requestBody, id: id]))

		return response
	}

	@Keyword
	def deleteCatalogRedeem(id) {
		def response = WS.sendRequestAndVerify(
				findTestObject(
				'Object Repository/FMC/Data Reference/Catalog Redeem/Delete Catalog Redeem',
				["id": id]
				))
		return response
	}
}
