package configureCoin

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS



public class requestBody {
	String expiryDate
	String year
}

public class Custom {

	@Keyword
	def createConfigureCoin(requestBody) {
		def response = WS.sendRequestAndVerify(
				findTestObject("Object Repository/FMC/Data Reference/Configure Koin/Post Configure Koin",
				[body: requestBody]))

		return response
	}

	@Keyword
	def getConfigureCoinList(sortBy, page, pageSize) {
		def response = WS.sendRequestAndVerify(
				findTestObject(
				'Object Repository/FMC/Data Reference/Configure Koin/Get Configure Koin - List',
				["sortBy":sortBy, "page": page, "pageSize": pageSize]
				))
		return response
	}

	@Keyword
	def editConfigureCoin(requestBody, id) {
		def response = WS.sendRequestAndVerify(
				findTestObject("Object Repository/FMC/Data Reference/Configure Koin/Edit Configure Koin",
				[body: requestBody, id: id]))

		return response
	}
}
