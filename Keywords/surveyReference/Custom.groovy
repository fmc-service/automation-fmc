package surveyReference

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class Custom {

	@Keyword
	def getSurveyList(sortBy, page, pageSize, surveyId='') {
		def response = WS.sendRequestAndVerify(
				findTestObject(
				'Object Repository/FMC/Data Reference/Survey Reference/Get Survey List',
				["sortBy": sortBy, "page": page, "pageSize": pageSize, "surveyId": surveyId]
				))
		return response
	}

	@Keyword
	def getDownloadList(sortBy='', surveyId='') {
		def response = WS.sendRequestAndVerify(
				findTestObject(
				'Object Repository/FMC/Data Reference/Survey Reference/Download Csv',
				["sortBy": sortBy, 'surveyId': surveyId]
				))
		return response
	}
}

