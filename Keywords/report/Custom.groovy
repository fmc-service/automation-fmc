package report

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class Custom {
	@Keyword
	def getTotalKoinCreation() {
		def response = WS.sendRequestAndVerify(
				findTestObject(
				'Object Repository/FMC/Report/Get Total Koin Creation'
				))
		return response
	}

	@Keyword
	def getTotalKoinRedemtion(startDate='', endDate='') {
		def response = WS.sendRequestAndVerify(
				findTestObject(
				'Object Repository/FMC/Report/Get Total Koin Redemtion',
				["startDate": startDate, "endDate": endDate]
				))
		return response
	}

	@Keyword
	def getTopTenSales(startDate='', endDate='') {
		def response = WS.sendRequestAndVerify(
				findTestObject(
				'Object Repository/FMC/Report/Get Top 10 Sales Force with Highest Koin',
				["startDate": startDate, "endDate": endDate]
				))
		return response
	}
	
	@Keyword
	def getSalesForceBWAchivement(startDate='', endDate='') {
		def response = WS.sendRequestAndVerify(
				findTestObject(
				'Object Repository/FMC/Report/Get Sales Force BW Achievement',
				["startDate": startDate, "endDate": endDate]
				))
		return response
	}
}
