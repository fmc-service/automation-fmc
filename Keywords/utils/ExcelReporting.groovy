package utils

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.util.IOUtils
import org.apache.poi.xssf.usermodel.XSSFClientAnchor
import org.apache.poi.xssf.usermodel.XSSFDrawing
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook


import internal.GlobalVariable

public class ExcelReporting {

	@Keyword
	def executionTime (String fileName){
		//Open File
		File sourceExcel = new File("Data Files/" + fileName + ".xlsx");
		FileInputStream fis = new FileInputStream(sourceExcel);

		XSSFWorkbook book = new XSSFWorkbook(fis)

		XSSFSheet sheet = book.getSheetAt(0)

		// Write Time execution
		Row row = sheet.createRow(2)
		Cell cell = row.createCell(0)
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		cell.setCellValue((String) "Waktu Eksekusi : " + now)

		for (int i = 0; i < 3; i++) {
			sheet.autoSizeColumn(i);
		}

		File targetExcel = new File("Data Files/" + fileName + ".xlsx");
		FileOutputStream os = new FileOutputStream(targetExcel)
		book.write(os)

		os.close();
		book.close();
	}

	@Keyword
	def excelReporting(String fileName, String object, String tcDetails, Boolean verify) {

		//Open File
		File sourceExcel = new File("Data Files/" + fileName + ".xlsx");
		FileInputStream fis = new FileInputStream(sourceExcel);

		XSSFWorkbook book = new XSSFWorkbook(fis)

		XSSFSheet sheet = book.getSheetAt(0)

		int lastRowNum = sheet.getLastRowNum();

		int numbering = lastRowNum - 4

		try {
			if(verify) {
				//Write TCID
				Row row = sheet.createRow(lastRowNum+1)
				Cell cell = row.createCell(0)
				cell.setCellValue((Integer) numbering)

				//Write tcDetails
				cell = row.createCell(1)
				cell.setCellValue((String) tcDetails)

				//Write Response
				cell = row.createCell(2)
				cell.setCellValue((String) object)

				//Write Status
				cell = row.createCell(3)
				cell.setCellValue((String) "PASSED")
			}
			else {
				//Write TCID
				Row row = sheet.createRow(lastRowNum+1)
				Cell cell = row.createCell(0)
				cell.setCellValue((String) numbering)

				//Write tcDetails
				cell = row.createCell(1)
				cell.setCellValue((String) tcDetails)

				//Write Response
				cell = row.createCell(2)
				cell.setCellValue((String) object)

				//Write Status
				cell = row.createCell(3)
				cell.setCellValue((String) "FAILED")
			}

		}
		catch (Exception e) {
			System.out.println(e);
		}

		for (int i = 0; i < 3; i++) {
			sheet.autoSizeColumn(i);
		}

		File targetExcel = new File("Data Files/" + fileName + ".xlsx");
		FileOutputStream os = new FileOutputStream(targetExcel)
		book.write(os)

		os.close();
		book.close();
	}

}
